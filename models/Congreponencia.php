<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congre_ponencia".
 *
 * @property integer $documento_id
 * @property string $documento_nombre
 * @property string $documento_archivo
 * @property string $documento_fechayhora
 * @property integer $documento_privilegios
 * @property string $documento_descripcion
 * @property integer $documento_activo
 */
class Congreponencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congresoponencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documento_nombre', 'documento_archivo', 'idtema' ], 'required'],
            [['documento_fechayhora'], 'safe'],            
            [['documento_privilegios', 'documento_activo' , 'idtema'], 'integer'],
            [['documento_nombre'], 'string', 'max' => 255],
            [['documento_archivo'], 'string'],
            [['documento_descripcion'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'documento_id' => 'Documento ID',
            'documento_nombre' => 'Nombre de la Ponencia',
            'documento_archivo' => 'Archivo Ms Power Point',
            'documento_fechayhora' => 'Fecha y Hora',
            'documento_privilegios' => 'Privilegios',
            'documento_descripcion' => 'Descripción',
            'documento_activo' => 'Documento Activo',
            'idtema' => 'Categoría',
        ];
    }
    
     public function getIdtema0()
    {
        return $this->hasOne(Congresotemas::className(), ['id' => 'idtema']);
    }
}
