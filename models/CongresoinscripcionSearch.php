<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congresoinscripcion;

/**
 * CongresoinscripcionSearch represents the model behind the search form about `app\models\Congresoinscripcion`.
 */
class CongresoinscripcionSearch extends Congresoinscripcion
{
    public function rules()
    {
        return [
            [['id', 'dni', 'idmodalidad', 'tipo1', 'tipo2', 'tipo3'], 'integer'],
            [['nombre', 'telefono', 'email', 'pais', 'ciudad', 'nombreponencia', 'cargo', 'institucion', 'direccion', 'nombre1', 'nombre2', 'nombre3'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congresoinscripcion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dni' => $this->dni,
            'idmodalidad' => $this->idmodalidad,
            'tipo1' => $this->tipo1,
            'tipo2' => $this->tipo2,
            'tipo3' => $this->tipo3,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pais', $this->pais])
            ->andFilterWhere(['like', 'ciudad', $this->ciudad])
            ->andFilterWhere(['like', 'nombreponencia', $this->nombreponencia])
            ->andFilterWhere(['like', 'cargo', $this->cargo])
            ->andFilterWhere(['like', 'institucion', $this->institucion])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'nombre1', $this->nombre1])
            ->andFilterWhere(['like', 'nombre2', $this->nombre2])
            ->andFilterWhere(['like', 'nombre3', $this->nombre3]);

        return $dataProvider;
    }
}
