<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Congre_ponencia;

/**
 * Congre_ponenciaSearch represents the model behind the search form about `app\models\Congre_ponencia`.
 */
class Congre_ponenciaSearch extends Congre_ponencia
{
    public function rules()
    {
        return [
            [['documento_id', 'documento_privilegios', 'documento_activo'], 'integer'],
            [['documento_nombre', 'documento_archivo', 'documento_fechayhora', 'documento_descripcion'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Congre_ponencia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'documento_id' => $this->documento_id,
            'documento_fechayhora' => $this->documento_fechayhora,
            'documento_privilegios' => $this->documento_privilegios,
            'documento_activo' => $this->documento_activo,
        ]);

        $query->andFilterWhere(['like', 'documento_nombre', $this->documento_nombre])
            ->andFilterWhere(['like', 'documento_archivo', $this->documento_archivo])
            ->andFilterWhere(['like', 'documento_descripcion', $this->documento_descripcion]);

        return $dataProvider;
    }
}
