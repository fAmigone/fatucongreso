<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congresomodalidad".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Congresoinscripcion[] $congresoinscripcions
 */
class Congresomodalidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congresomodalidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Modalidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongresoinscripcions()
    {
        return $this->hasMany(Congresoinscripcion::className(), ['idmodalidad' => 'id']);
    }
}
