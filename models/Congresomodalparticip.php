<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "congresomodalparticip".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Congresoinscripcion[] $congresoinscripcions
 */
class Congresomodalparticip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congresomodalparticip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Modalidad de Participación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCongresoinscripcions()
    {
        return $this->hasMany(Congresoinscripcion::className(), ['modalparticip' => 'id']);
    }
}
