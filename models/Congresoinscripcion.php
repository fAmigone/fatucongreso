<?php

namespace app\models;

use Yii;
use yii\swiftmailer\Mailer;
/**
 * This is the model class for table "congresoinscripcion".
 *
 * @property integer $id
 * @property integer $dni
 * @property string $nombre
 * @property integer $idmodalidad
 * @property string $telefono
 * @property string $email
 * @property string $pais
 * @property string $ciudad
 * @property string $nombreponencia
 * @property string $cargo
 * @property string $institucion
 * @property string $direccion
 * @property string $nombre1
 * @property integer $tipo1
 * @property string $nombre2
 * @property integer $tipo2
 * @property string $nombre3
 * @property integer $tipo3
 *
 * @property Congresomodalidad $tipo30
 * @property Congresoparticipacion $idmodalidad0
 * @property Congresomodalidad $tipo10
 * @property Congresomodalidad $tipo20
 */
class Congresoinscripcion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'congresoinscripcion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dni', 'nombre', 'idmodalidad', 'telefono', 'email', 'pais', 'ciudad', 'direccion' ], 'required'],
            [['dni', 'idmodalidad', 'tipo1', 'tipo2', 'tipo3', 'acreditado','modalparticip'], 'integer'],
            [['nombre', 'pais', 'ciudad', 'cargo', 'institucion', 'direccion'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['pagofecha', 'pagomonto'], 'string', 'max' => 15],
            [['nombreponencia', 'nombre1', 'nombre2', 'nombre3', 'observacion'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'DNI o Pasaporte',
            'nombre' => 'Nombre Completo',
            'idmodalidad' => 'Categoría de participante',
            'telefono' => 'Teléfono',
            'email' => 'Email',
            'pais' => 'País',
            'ciudad' => 'Ciudad',            
            'cargo' => 'Cargo',
            'institucion' => 'Institución',
            'direccion' => 'Dirección',
            'nombre1' => 'Nombre de la Ponencia (I)',
            'tipo1' => 'Modalidad',
            'nombre2' => 'Nombre de la Ponencia (II)',
            'tipo2' => 'Modalidad',
            'nombre3' => 'Nombre de la Ponencia (III)',
            'tipo3' => 'Modalidad',
            'modalparticip' => 'Modalidad de Participación',
            'pagofecha' => 'Fecha en que pago', 
            'pagomonto' => 'Monto abonado', 
            'observacion' => 'Observación', 
            'acreditado' => 'Acreditado'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo30()
    {
        return $this->hasOne(Congresomodalidad::className(), ['id' => 'tipo3']);
    }
    
    public function getModalparticip0()
    {
               return $this->hasOne(Congresomodalparticip::className(), ['id' => 'modalparticip']);
   }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmodalidad0()
    {
        return $this->hasOne(Congresoparticipacion::className(), ['id' => 'idmodalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo10()
    {
        return $this->hasOne(Congresomodalidad::className(), ['id' => 'tipo1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo20()
    {
        return $this->hasOne(Congresomodalidad::className(), ['id' => 'tipo2']);
    }
   
}
