<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congresoinscripcion $model
 */

$this->title = 'Informar Pago: ' . $model->nombre;

?>
<div class="congresoinscripcion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formadmin', [
        'model' => $model,
    ]) ?>

</div>
