<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congresoinscripcion $model
 */

$this->title = 'Por favor, ingrese sus datos.';
//$this->params['breadcrumbs'][] = ['label' => 'Congresoinscripcions', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congresoinscripcion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
