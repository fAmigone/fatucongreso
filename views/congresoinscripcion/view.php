<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congresoinscripcion $model
 */

$this->title = $model->nombre;
//$this->params['breadcrumbs'][] = ['label' => 'Congresoinscripcions', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congresoinscripcion-view">
    <center>
    <hr>
            <h1>VI Congreso Latinoamericano de Investigación Turística</h1>
            <h2>Número de inscripción: <?=$model->id?> </h2>
            
            <?=Html::img('http://www.fatu-uncoma.com.ar/congreso/images/congreso_condet_2014.jpg',['height'=>'200px'])?>
<h1>Hemos registrado sus datos.</h1>
    <h3>Su inscripción fué enviada al correo suministrado.</h3>
    <hr>
    
    </center>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [       
            'dni',
            'nombre',
            'modalparticip0.nombre',
            'idmodalidad0.nombre',
            'telefono',
            'email:email',
            'pais',
            'ciudad',           
            'cargo',
            'institucion',
            'direccion',
            'nombre1',
            'tipo10.nombre',
            'nombre2',
            'tipo20.nombre',
            'nombre3',
            'tipo30.nombre',

      
        ],
    ]) ?>

</div>
