<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\Congresoinscripcion $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congresoinscripcion-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'dni')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 100]) ?>
    
    
    <?= $form->field($model, 'modalparticip')->dropDownList(ArrayHelper::map(\app\models\Congresomodalparticip::find()->all(),'id','nombre'))?>           
    
    <?= $form->field($model, 'idmodalidad')->dropDownList(ArrayHelper::map(\app\models\Congresoparticipacion::find()->all(),'id','nombre'))?>           

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'pais')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'nombre1')->textInput(['maxlength' => 300]) ?>

    <?= $form->field($model, 'tipo1')->dropDownList(ArrayHelper::map(\app\models\Congresomodalidad::find()->all(),'id','nombre'), ['prompt' => '--Seleccionar si es Expositor--'])?>           

    <?= $form->field($model, 'nombre2')->textInput(['maxlength' => 300]) ?>

    <?= $form->field($model, 'tipo2')->dropDownList(ArrayHelper::map(\app\models\Congresomodalidad::find()->all(),'id','nombre'), ['prompt' => '--Seleccionar si es Expositor--'])?>           

    <?= $form->field($model, 'nombre3')->textInput(['maxlength' => 300]) ?>

    <?= $form->field($model, 'tipo3')->dropDownList(ArrayHelper::map(\app\models\Congresomodalidad::find()->all(),'id','nombre'), ['prompt' => '--Seleccionar si es Expositor--'])?>           

    <?= $form->field($model, 'cargo')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'institucion')->textInput(['maxlength' => 100]) ?>
    
    <?= $form->field($model, 'pagofecha')->textInput(['maxlength' => 15]) ?> 
 
    <?= $form->field($model, 'pagomonto')->textInput(['maxlength' => 15]) ?> 
 
    <?= $form->field($model, 'observacion')->textInput(['maxlength' => 300]) ?> 
    
    <?= $form->field($model, 'acreditado')->checkbox([1 =>'Acreditado']) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
