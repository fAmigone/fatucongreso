<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congresoinscripcion $model
 */

$this->title = $model->nombre;

?>
<div class="congresoinscripcion-view">
<?=Html::img('img/certificado-superior.jpg')?>                    
    <center> 
        <u><h2>
            <p class="text-center">  <?=$model->nombre ?> </p>
        </h2></u>
    </center>
    <h4>
        <p class="text-center"> Participó en calidad de <?=$model->modalparticip0->nombre ?> en el "VI Congreso Latinoamericano de Investigación Turística" que se llevo a cabo en la ciudad de Neuquén los días 25,26 y 27 de septiembre del 2014. </p>
    </h4>    
    <p>        
        <Br>
    </p>
<?=Html::img('img/certificado-inferior.jpg')?>
</div>    