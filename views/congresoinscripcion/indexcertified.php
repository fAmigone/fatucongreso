<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\CongresoinscripcionSearch $searchModel
 */

$this->title = 'Inscriptos';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congresoinscripcion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    
    ?>

    <p>
      
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,    
        'columns' => [     
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'dni',                        
            ['class' => 'yii\grid\ActionColumn',
               'template' => '{Imprimir}',
                'buttons' => [
                'Imprimir' => function ($url, $model) {
                return Html::a('Imprimir', ['/congresoinscripcion/viewprint', 'id' => $model->id]);
                     },
                ],
            ],
        ],
    ]); ?>

</div>
