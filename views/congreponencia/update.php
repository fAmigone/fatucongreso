<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congreponencia $model
 */

$this->title = 'Update Congreponencia: ' . $model->documento_id;
$this->params['breadcrumbs'][] = ['label' => 'Congreponencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->documento_id, 'url' => ['view', 'id' => $model->documento_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congreponencia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
