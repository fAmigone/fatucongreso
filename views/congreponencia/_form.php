<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\Congreponencia $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="congreponencia-form">

    <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'documento_nombre')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'documento_archivo')->fileInput() ?>

    <?= $form->field($model, 'documento_descripcion')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'idtema')->dropDownList(ArrayHelper::map(\app\models\Congresotemas::find()->all(),'id','nombre'))?>           
    <div
        class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
