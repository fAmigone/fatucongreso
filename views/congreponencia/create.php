<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congreponencia $model
 */

$this->title = 'Registrar Ponencia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congreponencia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
