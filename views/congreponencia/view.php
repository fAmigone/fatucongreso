<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Congreponencia $model
 */

$this->title = $model->documento_nombre ;

?>
<div class="congreponencia-view">
    <h1>        
        Su Ponencia fué registrada exitosamente.
    </h1>   
    
   
    <p>
        <?php //echo Html::a('Descargar', ['congreponencia/descargar','cambio_archivo' => $model->documento_archivo], ['class' => 'btn btn-primary']);?>
    </p>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'documento_nombre',
            'documento_archivo',
            'documento_fechayhora',            
            'documento_descripcion',
            'idtema0.nombre',
            
        ],
    ]) ?>

</div>
