<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\CongreponenciaSearch $searchModel
 */

$this->title = 'Ponencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congreponencia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Congreponencia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'documento_nombre',
            'documento_archivo',
            'documento_fechayhora',
            'idtema0.nombre',

            // 'documento_descripcion',
            // 'documento_activo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
