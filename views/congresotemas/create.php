<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congresotemas $model
 */

$this->title = 'Temas de Ponencias';
$this->params['breadcrumbs'][] = ['label' => 'Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congresotemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
