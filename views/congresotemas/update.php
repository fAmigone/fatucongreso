<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congresotemas $model
 */

$this->title = 'Update Congresotemas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Congresotemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="congresotemas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
