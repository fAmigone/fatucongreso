<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Congresomodalidad $model
 */

$this->title = 'Create Congresomodalidad';
$this->params['breadcrumbs'][] = ['label' => 'Congresomodalidads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="congresomodalidad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
