<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $content
 */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <center>        
        <title>VI Congreso Latinoamericano de Investigación Turística</title>                
        <?php $this->head() ?>
    </center>
</head>
<body>
    
        <?php $this->beginBody() ?>
        
            <hr>
            <h2>VI Congreso Latinoamericano de Investigación Turística</h2>
            <h3>Proceso de registración completo.</h3>
            <hr>
            <?=Html::img('http://www.fatu-uncoma.com.ar/congreso/images/congreso_condet_2014.jpg',['height'=>'200px'])?>
            <?php $this->head() ?>
        
         <?= $content ?>
   
        <?php $this->endBody() ?>
    
</body>
</html>
<?php $this->endPage() ?>
