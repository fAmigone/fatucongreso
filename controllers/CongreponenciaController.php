<?php

namespace app\controllers;

use Yii;
use app\models\Congreponencia;
use app\models\CongreponenciaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;






/**
 * CongreponenciaController implements the CRUD actions for Congreponencia model.
 */
class CongreponenciaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Congreponencia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CongreponenciaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Congreponencia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Congreponencia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
  

    
      public function actionDescargar($cambio_archivo)
    {

                $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'ponencias';   
                //Especificamos la ruta en donde se encuentran los archivos 
                //Yii:: Es una colección de funciones auxiliares útiles para Yii Framework
                //$app->basePath Nos da el directorio raiz de la aplicacion
                $file = $path.$cambio_archivo;
                //Concatena la ruta con el archivo a descargar($cambio_archivo)
                
                header('Content-disposition: attachment; filename='.$file);
                //Content-disposition proporciona un nombre de fichero recomendado y fuerza al navegador el mostarar el diálogo para guardar el fichero
                header("Content-Type: application/force-download");                
                
                $dirInfo = pathinfo($file); 
                //pathinfo Devuelve información acerca de la ruta de un fichero como un array asociativo, o bien como un string
                $nombre = $dirInfo['basename'];                                            
                $ext = $dirInfo['extension']; 
                //Asigna la extension del archivo a una variable
                                
                if(strtolower($ext) == 'ppt') //Pasa la extension a minuscula y la compara si es igual a pdf
                    header('application/vnd.ms-powerpoint'); //Si es igual a pdf abre la aplicacion para mostrar el pdf en la pagina                                                
                            
                header("Content-disposition: attachment; filename=".$nombre);
                //Content-disposition proporciona un nombre de fichero recomendado y fuerza al navegador el mostarar el diálogo para guardar el fichero 
                
                $file = file_get_contents($file);
                //file_get_contents() es la manera preferida de transmitir el contenido de un fichero a una cadena. Usa técnicas de mapeado de memoria, para mejorar el rendimiento
                
                echo $file;
                die;         //Muere la funcion(Termina)         

            }
    
     public function actionCreate()
    {
        $model = new Congreponencia;
        //Crea una nueva instancia de la clase EdmDocumento
        $band = false;
        //Crea una bandera con valor falso

        //if(isset($_POST['EdmDocumento'])){ //Entra al if si hay alguna variable enviada por post con el nombre 'EdmDocumento'
        if(isset($_POST['Congreponencia'])){ //Entra al if si hay alguna variable enviada por post con el nombre 'EdmDocumento'
    
            $model->load(Yii::$app->request->post());
            //A la instancia creada $model se le asigna todos los datos que vienen por post del formulario
            
            $file = UploadedFile::getInstance($model,'documento_archivo');
            //UploadedFile es una clase que representa la informacion de un archivo subido
            //A $file le asignamos la informacion del archivo subido
            
            if(is_object($file)){ //Comprueba si la variable $file es un objeto                   
                
                $extenssion = substr($file->name, -4); //Selecciona los ultimos 4 caracteres para obtener la extension y la asigna a la variable
                $nombreCifrado = $model->documento_nombre.microtime(); //Cifra en md5 la variable entregada por la funcion microtime y la asigna a la variable
                $nombreCifrado .= $extenssion ; //Adiciona a la variable anterior la extension del archivo
                $file->name = $nombreCifrado;//md5($file->name).$extenssion;
                $carpetatema = $model->idtema0->nombre;
                $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'ponencias'.DIRECTORY_SEPARATOR.$carpetatema;
                //Especificamos la ruta en donde se encuentran los archivos 
                //Yii:: Es una colección de funciones auxiliares útiles para Yii Framework
                //$app->basePath Nos da el directorio raiz de la aplicacion
                
                if(!is_dir($path)) //is_dir indica si el nombre de la ruta es un directorio, si es falso entra en el if 
                mkdir($path); //Crea un directorio con la ruta dada si no existe
                                     
                /* --------------------------------- */                                      
                $url   = $path.DIRECTORY_SEPARATOR.$file->name; //url definitiva al archivo (Donde guarda el archivo)                                                            
                $array = pathinfo($url); //Devuelve la informacion del URL como un array asociativo y la asigna a la variable
                $ext   = $array['extension']; //Asigna la extension a una variable
                $ext   = strtolower($ext); //Pasa a minuscula la extension                     
                /* --------------------------------- */                                      
                 
                if(empty($ext) or (($ext != 'ppt') and ($ext != 'pps') and ($ext != 'pptx'))){ 
                    //Si la extension esta vacia o es diferente de alguna de las extensiones escritas entra por el if
                      throw new \yii\web\HttpException(204, " Solo puede subir archivos con extension (.ppt) o (.pps) o (.pptx)");
                      //Muestra un error por subir un archivo con extension no  valida
                }
                else{                         
                     $file->saveAs($url); //Si entra por el else guarda el archivo en la ruta especificada
                     $band = true; //Cambia la bandera con el valor true
                }     
                                      
                /* --------------------------------------- */
                $model->documento_archivo    = $file->name; //Al modelo con el tag documento_archivo se le asigna el nombre del archivo

                //echo($model->documento_archivo); exit;                 
                //$model->documento_usuario    = Yii::$app->user->getState('idUsuario');
                
                //$model->documento_usuario    = Yii::$app->user->getId(); //Al modelo con el tag documento_usuario se le asigna el id del usuario
                $model->documento_activo     = 1;  //Al modelo con el tag documento_activo se le asigna 1 y siempre se pone como activo
                $model->documento_fechayhora = date('Y-m-d H:i:s'); //Asignamos a documento_fechayhora el el tiempo y fecha con la funcion date
                //$model->documento_proyecto = $idProyecto; //Al modelo con el tag documento_proyecto se le asigna el id del proyecto que entra por post
                
                //especifica los privilegios al crear los archivos;                
                //if (Yii::$app->user->can('cliente')) { //Si el usuario es cliente entra al if
                //    $model->documento_privilegios = 1; //Le asigna a documento_privilegios el valor 1(Publico)

                //}
                //else {
                //    $model->documento_privilegios = 2; //Le asigna a documento_privilegios el valor 2(Privado)
                //};                                        
            };                  
        }
                    if ($band && $model->save()){ //Si $band es true y pudo guardar los datos del modelo entra por el if
                        return $this->redirect(['view', 'id' => $model->documento_id]);} //Redirecciona a la vista del documento creado
                    else {
                        return $this->render('create', [ //Si entra por el else vuelve a renderizar la vista create
                            'model' => $model,
                        ]);
                    }                       
    }
    
    /**
     * Updates an existing Congreponencia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->documento_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Congreponencia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Congreponencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Congreponencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Congreponencia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
