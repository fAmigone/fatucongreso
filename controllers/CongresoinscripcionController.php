<?php

namespace app\controllers;

use Yii;
use app\models\Congresoinscripcion;
use app\models\CongresoinscripcionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use mPDF;

/**
 * CongresoinscripcionController implements the CRUD actions for Congresoinscripcion model.
 */
class CongresoinscripcionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Congresoinscripcion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CongresoinscripcionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
  public function actionIndexadmin()
    {
        $searchModel = new CongresoinscripcionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('indexadmin', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    
     public function actionIndexcertified()
    {
        $searchModel = new CongresoinscripcionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('indexcertified', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    public function actionIndexexterno()
    {
        $searchModel = new CongresoinscripcionSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('indexexterno', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    /**
     * Displays a single Congresoinscripcion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewprint($id)
    {        
        $model = $this->findModel($id);
        if ($model->acreditado ==1)
        {
            $mpdf = new mPDF('c', 'A4-L');
            $mpdf->WriteHTML($this->render('viewCertificado', [
                'model' => $model,
            ]));
            $mpdf->Output();
        }        
        else {
            return ($this->render('viewCertificadoError', [
                'model' => $model,
            ]));
        }
        
    }
    /**
     * Creates a new Congresoinscripcion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Congresoinscripcion;
 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           Yii::$app->mail->compose('html',['$model'=>$model])
                ->setFrom('secretaria.investigacion@fatu.uncoma.edu.ar')
                ->setTo($model->email)
                ->setSubject('VI Congreso Latinoamericano de Investigación Turística')
                ->setHtmlBody($this->render('view', [
            'model' => $model,
        ]))
                ->send();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
public function actionMail()
    {
// Yii::$app->mail->compose('html',[])
//    ->setFrom('secretaria.investigacion@fatu.uncoma.edu.ar')
//    ->setTo('fede.amigone@gmail.com')
//    ->setSubject('esto es una mierda')
//    ->send();
    
Yii::$app->mail->compose('html',[])
                ->setFrom('secretaria.investigacion@fatu.uncoma.edu.ar')
                ->setTo('fede.amigone@gmail.com')
                ->setSubject('VI Congreso Latinoamericano de Investigación Turística')
                ->send();    
        
    }
     
 
 
    
    /**
     * Updates an existing Congresoinscripcion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['indexadmin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Congresoinscripcion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['indexadmin']);
    }

    /**
     * Finds the Congresoinscripcion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Congresoinscripcion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Congresoinscripcion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
